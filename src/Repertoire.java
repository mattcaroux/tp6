import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Repertoire{
    private Map<String, List<String>> repertoire;

    public Repertoire(Map<String, List<String>> repertoire){
        this.repertoire = repertoire;
    }
    public void ajouteContact(String nom, String numero){
        List<String> listeNumero = new ArrayList<String>();
        listeNumero.add(numero);
        if(!repertoire.containsKey(nom)){
            repertoire.put(nom, listeNumero);
        }
    }
}